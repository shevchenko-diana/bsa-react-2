import React, {useState} from "react";
import PropTypes from "prop-types";
import '@fortawesome/fontawesome-free/css/all.min.css'
import './styles.css'
import {equalDates, formatDate, isToday} from "../../helpers/date";

const MessageList = ({messages, ownUserId, likeMsg, editMsg, deleteMsg}) => {
    let lastDate = messages.length > 0 ? messages[0].createdAt : null;

    const doNeedDivider = (date) => {
        if (!equalDates(lastDate, date)) {
            lastDate = date
            return true
        }
        lastDate = date
        return false

    }
    const divider = date => {
        let text = ''
        if (isToday(date)) {
            text = 'Today'
        } else {
            text = formatDate(date,
                {
                    year: 'numeric',
                    month: 'short',
                    day: "numeric",
                })
        }
        return <div className={'divider'}> {text}</div>
    }
    return <div className={'message-list'}>

        {messages.map(msg =>
            (<>
                {doNeedDivider(msg.createdAt) ? divider(msg.createdAt) : null}
                <Message
                    key={msg.id}
                    msg={msg}
                    ownMsg={ownUserId === msg.userId}
                    likeMsg={likeMsg}
                    editMsg={editMsg}
                    deleteMsg={deleteMsg}
                />
            </>)
        )}
    </div>


}
const Message = ({msg, ownMsg, likeMsg, editMsg, deleteMsg}) => {
    const [editingMsg, setEditingMsg] = useState('')

    const editingMsgInput = () => {
        return (
            <div className='message-edit'>
                    <textarea
                        onChange={e => setEditingMsg(e.target.value)}
                        value={editingMsg}/>
                <button type={'submit'} onClick={
                    () => {
                        editMsg(msg.id, editingMsg)
                        setEditingMsg('')
                    }

                }>Submit
                </button>
                <button type={"reset"} onClick={() => setEditingMsg('')}>Cancel</button>
            </div>)
    }
    // console.log(msg)
    return (
        <div className={'message-item ' + (ownMsg ? 'own-message' : '')}>
            {!ownMsg ? <img src={msg.avatar}/> : null}
            {editingMsg !== ''
                ? editingMsgInput()
                : <>
                    <div>{msg.text}</div>
                    <span>{formatDate(msg.createdAt)}</span>
                    {ownMsg
                        ? (<div>
                            <i
                                className="fas fa-trash message-icon"
                                onClick={() => deleteMsg(msg.id)}
                            />
                            <i
                                className="fas fa-cog message-icon"
                                onClick={() => setEditingMsg(msg.text)}
                            />
                        </div>)
                        : <i onClick={() => likeMsg(msg.id)}>
                            {msg.isLiked ?
                                <span className='like'> &#10084;&#65039;</span> :
                                <span className='like'>&#x1F90D;</span>}
                        </i>
                    }
                </>
            }


        </div>
    )
}


Message.propTypes = {
    msg: PropTypes.object.isRequired,
    ownMsg: PropTypes.bool.isRequired,
    likeMsg: PropTypes.func.isRequired,
    editMsg: PropTypes.func.isRequired,
};

export default MessageList;
