import React from "react";
import PropTypes from 'prop-types';
import './styles.css'
import {formatDate} from "../../helpers/date";

const Header = ({
                    chatName: name, numOfUsers, numOfMessages, timestamp
                }) => {
    return (
        <header>
            <span>{name}</span>
            <span>{numOfUsers} participant(s)</span>
            <span>{numOfMessages} Messages</span>
            <span>last message at {formatDate(timestamp)}</span>

        </header>
    )
};

Header.propTypes = {
    chatName: PropTypes.string.isRequired,
    numOfUsers: PropTypes.number.isRequired,
    numOfMessages: PropTypes.number.isRequired,
    lastMsgTimestamp: PropTypes.number,
};

export default Header;
