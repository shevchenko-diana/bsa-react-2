import React from "react";
import Header from "../../components/Header";
import MessageList from "../../components/MessageList";
import MessageInput from "../../components/MessageInput";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import './styles.css'
import {addMessage, deleteMessage, editMessage, likeMessage, loadChatInfo, loadMessages} from './actions'
import Spinner from "../../components/Spinner";

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            ownUser: {},
        }
        this.fetchMessages = this.fetchMessages.bind(this)
    }

    componentWillMount() {
        this.fetchMessages()
    }

    fetchMessages = () => {
        fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
            .then(res => res.json())
            .then(messages => {
                const users = new Set()
                messages.forEach(msg => users.add(msg.userId))

                console.log('fetch loadMessages')
                this.props.loadMessages(messages)
                this.props.loadChatInfo({
                    numOfUsers: users.size,
                    timestamp: messages[messages.length - 1].createdAt,
                    numOfMessages: messages.length,
                    chatName: 'Launderers',
                    ownUser: {
                        userId: messages[0].userId,
                        user: messages[0].user,
                    }
                })

                this.setState({
                    isLoading: false,
                    ownUser: {
                        userId: messages[0].userId,
                        user: messages[0].user,
                    }
                })
            })
    }


    render() {

        return (
            this.state.isLoading ? <Spinner/> :
                <div className={'chat-container'}>

                    <Header {...this.props.chatInfo}/>
                    <MessageList
                        messages={this.props.messages}
                        ownUserId={this.state.ownUser.userId}
                        likeMsg={this.props.likeMessage}
                        editMsg={this.props.editMessage}
                        deleteMsg={this.props.deleteMessage}
                    />
                    <MessageInput submitMsg={this.props.addMessage}/>
                </div>
        )
    }
}


const mapStateToProps = state => {
    return ({
        messages: state.chat.messages,
        chatInfo: state.chat.chatInfo
    });
}
const actions = {loadChatInfo, loadMessages, deleteMessage, likeMessage, editMessage, addMessage}
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chat);
