import {ADD_MESSAGE, DELETE_MESSAGE, EDIT_MESSAGE, LIKE_MESSAGE, LOAD_CHAT_INFO, LOAD_MESSAGES} from './actionTypes';
import {v4 as uuidv4} from "uuid";

const addMessageAction = message => ({
    type: ADD_MESSAGE,
    message
});
const loadMessagesAction = messages => ({
    type: LOAD_MESSAGES,
    messages
});

const loadChatInfoAction = chatInfo => ({
    type: LOAD_CHAT_INFO,
    chatInfo
});

const deleteMessageAction = messages => ({
    type: DELETE_MESSAGE,
    messages
});
const editMessageAction = messages => ({
    type: EDIT_MESSAGE,
    messages
});

const likeMessageAction = messages => ({
    type: LIKE_MESSAGE,
    messages
});

export const loadMessages = messages => async (dispatch) => {
    console.log('loadMessages')
    dispatch(loadMessagesAction(messages))
}
export const loadChatInfo = chatInfo => async (dispatch) => {
    console.log('loadChatInfo')
    dispatch(loadChatInfoAction(chatInfo))
}
export const likeMessage = id => (dispatch, getRootState) => {
    const {chat: {messages}} = getRootState();
    const msgs = messages.map(msg => {
        if (msg.id === id) {
            msg.isLiked = !msg.isLiked
        }
        return msg
    })

    dispatch(likeMessageAction(msgs))

}
export const addMessage = (text) => (dispatch, getRootState) => {
    const {chat: {messages, chatInfo}} = getRootState();
    console.log(' { messages, chatInfo } ', messages, chatInfo)
    const newMsg = {
        createdAt: new Date().toISOString(),
        editedAt: '',
        id: uuidv4(),
        text,
        user: chatInfo.ownUser.user,
        userId: chatInfo.ownUser.userId,
    }
    dispatch(loadChatInfoAction({
        ...chatInfo,
        numOfMessages: messages.length + 1,
        timestamp: newMsg.createdAt
    }))

    dispatch(addMessageAction(newMsg))

}

export const editMessage = (id, text) => (dispatch, getRootState) => {
    const {chat: {messages}} = getRootState();
    const newMsgs = messages.map(msg => {
        if (msg.id === id) {
            msg.text = text
        }
        return msg
    })

    dispatch(editMessageAction(newMsgs))
}

export const deleteMessage = (id) => (dispatch, getRootState) => {
    const {chat: {messages}} = getRootState();
    const newMsgs = messages.filter(msg => msg.id !== id)


    dispatch(deleteMessageAction(newMsgs))
}
