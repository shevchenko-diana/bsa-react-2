import {combineReducers, applyMiddleware,createStore} from 'redux';
import chatReducer from './containers/Chat/reducer';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
const initialState = {

};

const middlewares = [
    thunk,
];

const composedEnhancers = composeWithDevTools(
    applyMiddleware(...middlewares)
);

const rootReducer = combineReducers({
    chat : chatReducer,
});

const store = createStore(
    rootReducer,
    initialState,
    composedEnhancers
);

export default store;
